﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace phoneUI
{
    public partial class MainPage : ContentPage
    {
        public string num = "";

        public MainPage()
        {
            InitializeComponent();
            number1.Text = "1\n*";
            number2.Text = "2\nABC";
            number3.Text = "3\nDEF";
            number4.Text = "4\nGHI";
            number5.Text = "5\nJKL";
            number6.Text = "6\nMNO";
            number7.Text = "7\nPQRS";
            number8.Text = "8\nTUV";
            number9.Text = "9\nWXYZ";
            number0.Text = "0\n+";
            end.IsVisible = false;
        }



        public void number1Click(object sender, EventArgs e)
        {
            num = num + "1";
            label1.Text = num;
        }
        public void number2Click(object sender, EventArgs e)
        {
            num = num + "2";
            label1.Text = num;
        }
        public void number3Click(object sender, EventArgs e)
        {
            num = num + "3";
            label1.Text = num;
        }
        public void number4Click(object sender, EventArgs e)
        {
            num = num + "4";
            label1.Text = num;
        }
        public void number5Click(object sender, EventArgs e)
        {
            num = num + "5";
            label1.Text = num;
        }
        public void number6Click(object sender, EventArgs e)
        {
            num = num + "6";
            label1.Text = num;
        }
        public void number7Click(object sender, EventArgs e)
        {
            num = num + "7";
            label1.Text = num;
        }
        public void number8Click(object sender, EventArgs e)
        {
            num = num + "8";
            label1.Text = num;
        }
        public void number9Click(object sender, EventArgs e)
        {
            num = num + "9";
            label1.Text = num;
        }
        public void number0Click(object sender, EventArgs e)
        {
            num = num + "0";
            label1.Text = num;
        }
        public void callClick(object sender, EventArgs e)
        {
            call.IsVisible = false;
            end.IsVisible = true;
        }
        public void clearClick(object sender, EventArgs e)
        {
            num = "";
            label1.Text = num;
        }
        public void endClick(object sender, EventArgs e)
        {
            call.IsVisible = true;
            end.IsVisible = false;
            num = "";
            label1.Text = num;
        }
            
    }
}
